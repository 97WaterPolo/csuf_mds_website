<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 4/1/2020
 * Time: 1:03 AM
 */
if (!isset($_GET['code'])){
    session_start();
    $_SESSION['error'] = 'No verification code given! Please ensure you registered properly!';
    header("Location: ../register.php");
    return;
}

require_once 'framework/connect.php';

$stmt= $link->prepare("SELECT * FROM `MDS`.`User` WHERE ID=(SELECT ID FROM `MDS`.`Registration` WHERE `Code`=?)");
$stmt->bind_param("s", $_GET['code']);
$stmt->execute();
$result = $stmt->get_result();
if ($result->num_rows === 0){
    $_SESSION['error'] = 'Couldn\'t find an account associated with that verification code. Please login, reset password, register, or contact support!!';
    header("Location: ../register.php");
    return;
}

$row = $result->fetch_assoc();
$userId = $row['ID'];
$stmt= $link->prepare("UPDATE `MDS`.`User` SET `Verified`=1 WHERE `ID`=".$userId);
$stmt->execute();

$stmt= $link->prepare("DELETE FROM `MDS`.`Registration` WHERE `ID`=".$userId);
$stmt->execute();

$stmt->close();

session_start();
$_SESSION['success'] = 'Thank you '.$row['First'].' '.$row['Last'].' for verifying your email! You now have full access to use the MDS Console!';
header("Location: ../login.php");