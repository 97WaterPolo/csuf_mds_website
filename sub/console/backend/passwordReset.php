<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 4/1/2020
 * Time: 3:26 AM
 */


if (!isset($_GET['code'])){
    session_start();
    $_SESSION['error'] = 'No password reset code given! Please ensure you reset your password properly!';
    header("Location: ../forgot-password.php");
    return;
}

require_once 'framework/connect.php';
require_once 'framework/MDS_Mailer.php';

$stmt= $link->prepare("SELECT * FROM `MDS`.`User` WHERE ID=(SELECT ID FROM `MDS`.`Registration` WHERE `Code`=?)");
$stmt->bind_param("s", $_GET['code']);
$stmt->execute();
$result = $stmt->get_result();
if ($result->num_rows === 0){
    $_SESSION['error'] = 'Couldn\'t find the account associated with this password reset link.';
    header("Location: ../forgot-password.php");
    return;
}


$code = md5(uniqid(rand(), true));
$newPassword = substr($code, 0, 8);
$password = password_hash($newPassword, PASSWORD_DEFAULT);


$row = $result->fetch_assoc();
$stmt= $link->prepare("UPDATE `MDS`.`User` SET `Password`=?, `Verified`=1 WHERE `ID`=".$row['ID']);
echo "UPDATE `MDS`.`User` SET `Password`=? AND `Verified`=1 WHERE `ID`=".$row['ID'];
$stmt->bind_param("s", $password);
$stmt->execute();

$stmt= $link->prepare("DELETE FROM `MDS`.`Registration` WHERE `ID`=".$row['ID']);
$stmt->execute();
$stmt->close();


$msg = 'Hi '.$row['First'].' '.$row['Last'].',<br><br> Your password for Titan MDS has been reset.<br><br>
<b>Your new password is: </b><h2>'.$newPassword.'</h2>';

new MDS_Mailer($row['Email'], 'MDS Password Reset', $msg);



session_start();
$_SESSION['success'] = 'Thank you '.$row['First'].' '.$row['Last'].' for resetting your password. A temporary password has been emailed to you, please log in using that!';
header("Location: ../login.php");