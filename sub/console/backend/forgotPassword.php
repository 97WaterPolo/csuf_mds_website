<?php
if (empty($_POST['email'])) {
    session_start();
    $_SESSION['error'] = 'No valid email address was entered!';
    header("Location: ../forgot-password.php");
    return;
}


require_once "framework/connect.php";
require_once "framework/MDS_Mailer.php";

session_start();
$stmt= $link->prepare("SELECT * FROM `MDS`.`User` WHERE `Email`=?");
$stmt->bind_param("s", mysqli_escape_string($link, $_POST['email']));
$stmt->execute();
$result = $stmt->get_result();
if ($result->num_rows != 0){
    $row = $result->fetch_assoc();
    $code = md5(uniqid(rand(), true));

    $stmt= $link->prepare("INSERT INTO `MDS`.`Registration` (`ID`, `Code`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `Code`=?;");
    $stmt->bind_param("iss", $row['ID'], $code, $code);
    $stmt->execute();
    $stmt->close();

    $msg = 'Hello '.$row['First'].' '.$row['Last'].',<br><br>Your password for Titan MDS has been requested to be
reset.<br><br>Please click the following link <a href="http://console.titan-mds.com/backend/passwordReset.php?code='.$code.'">
MDS Password Reset</a><br><br>If link doesn\'t work please copy
    and paste the following url into your browser!http://console.titan-mds.com/backend/passwordReset.php?code='.$code;

    new MDS_Mailer($row['Email'], 'MDS Password Reset Link', $msg);

    $_SESSION['success'] = 'Your email has been found, a password reset link has been sent to '.$_POST['email'].'!';

}else
    $_SESSION['error'] = 'No account found under that email';


header("Location: ../login.php")

?>