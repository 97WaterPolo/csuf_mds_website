<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 4/1/2020
 * Time: 3:40 AM
 */

print_r($_POST);
session_start();
if (empty($_POST['email'])){
    $_SESSION['error'] = 'No email specified!';
    header("Location: ../login.php");
    return;
} else if (empty($_POST['password'])){
    $_SESSION['error'] = 'No password specified!';
    header("Location: ../login.php");
    return;
}

require_once 'framework/connect.php';
require_once 'framework/MDS_Mailer.php';

$stmt= $link->prepare("SELECT * FROM `MDS`.`User` WHERE `Email`=?");
$stmt->bind_param("s", $_POST['email']);
$stmt->execute();
$result = $stmt->get_result();
if ($result->num_rows === 0){
    $_SESSION['error'] = 'Couldn\'t find any account with that email';
    header("Location: ../login.php");
    return;
}
$row = $result->fetch_assoc();

if (password_verify($_POST['password'], $row['Password'])){
    $_SESSION['AUTH_USER'] = $row;
    header("Location: ../index.php");
    return;
}else{
    $_SESSION['error'] = 'Incorrect password! Please try again. If you forgot your password please reset it below!';
    header("Location: ../login.php");
    return;
}










