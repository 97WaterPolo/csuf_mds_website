<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 3/31/2020
 * Time: 6:06 PM
 */
include_once "framework/connect.php";
include_once "framework/MDS_Mailer.php";

print_r($_POST);
session_start();
if (empty($_POST['firstName']) || empty($_POST['lastName']) || empty($_POST['email']) || empty($_POST['password']) || empty($_POST['number'])){
    $_SESSION['error'] = 'One of your fields was empty. Please try again!';
    header("Location: ../register.php");
    return;
}else if (strlen($_POST['password']) < 8){
    $_SESSION['error'] = 'Password not strong enough. Min. of 8 characters [A-Z][a-z][0-9][Special Characters]';
    header("Location: ../register.php");
    return;
}else if ($_POST['password'] !== $_POST['passwordRepeat']){
    $_SESSION['error'] = 'Passwords do not match! Try again!';
    header("Location: ../register.php");
    return;
}

$firstName = mysqli_real_escape_string($link, $_POST['firstName']);
$lastName = mysqli_real_escape_string($link, $_POST['lastName']);
$email = mysqli_real_escape_string($link, $_POST['email']);
$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
$number = mysqli_real_escape_string($link, $_POST['number']);
$accountType = isset($_POST['account']) ? 1 : 0;

$stmt= $link->prepare("SELECT * FROM `MDS`.`User` WHERE `Email`=?");
$stmt->bind_param("s", $email);
$stmt->execute();
if ($stmt->get_result()->num_rows != 0){
    $_SESSION['error'] = 'You already have an account with that email. Please log in!';
    header("Location: ../login.php");
    return;
}
$stmt->close();

$stmt= $link->prepare("INSERT INTO `MDS`.`User` (`AccountType`, `First`, `Last`, `Phone`, `Email`, `Password`) VALUES (?, ?, ?, ? ,? ,?);");
$stmt->bind_param("isssss", $accountType, $firstName, $lastName, $number, $email, $password);
$stmt->execute();
$stmt->close();

$newUserId = mysqli_insert_id($link);
$code = md5(uniqid(rand(), true));


$stmt= $link->prepare("INSERT INTO `MDS`.`Registration` (`ID`, `Code`) VALUES (?, ?);");
$stmt->bind_param("is", $newUserId, $code);
$stmt->execute();
$stmt->close();


$msg = 'Hello '.$firstName.' '.$lastName.',<br><br> We are happy you chose us for your digital advertising needs.<br><br>In order to get started please click the link (expires in 24 hours)
below to verify your account. Once verified please log in and view our console to continue on your MDS journey, whether as an MDS Customer or an
Advertiser!<br><br>Please click the following link <a href="http://console.titan-mds.com/backend/verification.php?code='.$code.'">MDS Verification Link</a><br><br>If link doesn\'t work please copy
and paste the following url into your browser! http://console.titan-mds.com/verification.php?code='.$code;

new MDS_Mailer($email, "MDS Account Verification", $msg);



$_SESSION['success'] = 'Account has been created! Please check your email for verification (Please check spam folder)!';

header("Location: ../login.php");