<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 4/1/2020
 * Time: 11:20 AM
 */

if (session_status() == PHP_SESSION_NONE)
    session_start();

if (!isset($_SESSION['AUTH_USER'])){
    $_SESSION['error'] = 'You are currently logged out. Please log in to continue!';
    header("Location: ".dirname(__DIR__).'/../login.php');
    return;
}