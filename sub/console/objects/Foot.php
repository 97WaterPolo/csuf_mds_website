<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 4/1/2020
 * Time: 11:42 AM
 */

?>

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']));?>/../vendor/jquery/jquery.min.js"></script>
<script src="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']));?>/../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']));?>/../vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']));?>/../js/sb-admin-2.min.js"></script>
