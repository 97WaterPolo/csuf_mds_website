<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 4/3/2020
 * Time: 2:12 AM
 */

?>

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->
