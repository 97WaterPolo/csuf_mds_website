<!DOCTYPE html>
<html lang="en">

<?php
include(dirname(__DIR__).'/objects/Head.php');
?>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <?php
    include(dirname(__DIR__).'/objects/MenuBar.php');
    ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php
            include(dirname(__DIR__).'/objects/NavBar.php');
            ?>
